package com.example.cms_be.service;

import com.example.cms_be.model.Customer;
import com.example.cms_be.model.User;
import com.example.cms_be.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<Customer> getListUsers() {
        return userRepository.findAll();
    }

    public List<Customer> getUsersById (Long id) {
        return userRepository.getUsersById(id);
    }
}
