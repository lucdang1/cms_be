package com.example.cms_be.repository;

import com.example.cms_be.model.Customer;
import com.example.cms_be.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface UserRepository extends JpaRepository<Customer, Long> {

    @Query(value = "select c from Customer c where c.id = :id")
    List<Customer> getUsersById(Long id);
}
