package com.example.cms_be.repository;

import com.example.cms_be.model.Customer;
import com.example.cms_be.model.Product;
import org.apache.catalina.mbeans.SparseUserDatabaseMBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query("select p from Product p where p.id = :idbien")
    List<Product> getProductByLucdv(Long idbien);

}
