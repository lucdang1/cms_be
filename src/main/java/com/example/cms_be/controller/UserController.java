package com.example.cms_be.controller;

import com.example.cms_be.model.Customer;
import com.example.cms_be.model.User;
import com.example.cms_be.service.UserService;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonAnyFormatVisitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("get-user")
    public ResponseEntity<List<Customer>> getUser(){
        return new ResponseEntity<>(userService.getListUsers(), HttpStatus.OK);
//        return "string-string";
    }


    @GetMapping(value = "get-user/{id}")
    public ResponseEntity<List<Customer>> getUser(@PathVariable Long id){
        return new ResponseEntity<>(userService.getUsersById(id), HttpStatus.OK);
    }
}
