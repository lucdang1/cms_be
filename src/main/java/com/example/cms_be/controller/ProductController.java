package com.example.cms_be.controller;

import com.example.cms_be.model.Product;
import com.example.cms_be.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping(value = "get-product-lucdv")
    public ResponseEntity<List<Product>> getListProduct(){
        return new ResponseEntity<>(productRepository.getProductByLucdv(1L), HttpStatus.OK);
    }

}
